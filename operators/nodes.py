import bpy, time
from bpy.types import Operator
from ..util import debug, realpath, view3D_mat_update
from ..engine import kill_preview

class RefreshTextureNode(Operator):
    bl_idname = "corona.refresh_image"
    bl_label = "Refresh Image"

    node = bpy.props.StringProperty(name="Selected Node")

    def execute(self, context):
        mat = context.object.active_material
        node_tree = mat.node_tree
        node = node_tree.nodes[self.node]

        # debug("Texture path '" + node.tex_path + "'", dict(context.blend_data.images))
        if node.get_texture_path():
            img = context.blend_data.images.load(realpath(node.tex_path), check_existing=True)
            img.reload()
            view3D_mat_update(self, context, custom_node = node)

            #change viewport shading to textured
            for area in bpy.context.screen.areas:
                if area.type == 'VIEW_3D':
                    for space in area.spaces:
                        if space.type == 'VIEW_3D':
                            space.viewport_shade = 'TEXTURED'

        return {'FINISHED'}


class RefreshPreview(Operator):
    bl_idname = "corona.refresh_preview"
    bl_label = "Refresh Preview"

    def execute(self, context):
        kill_preview()
        #view3D_mat_update(self, context)
        return {'FINISHED'}


class RefreshBothOp(bpy.types.Operator):
    """Reload the image in blender and in corona"""
    # Need to do it this way so the image file doesn't get locked
    bl_idname = "corona.refresh_both"
    bl_label = "Show in Viewport"


    node = bpy.props.StringProperty(name="Selected Node")
    _timer = None
    _counter = 0

    def modal(self, context, event):
        if event.type == 'TIMER':
            if self._counter == 0:
                bpy.ops.corona.refresh_image(node = self.node)
            elif self._counter == 1:
                bpy.ops.corona.refresh_preview()
                self.cancel(context)
                return {'CANCELLED'}

            self._counter += 1

        return {'PASS_THROUGH'}

    def execute(self, context):
        wm = context.window_manager
        self._timer = wm.event_timer_add(0.01, context.window)
        wm.modal_handler_add(self)
        return {'RUNNING_MODAL'}

    def cancel(self, context):
        wm = context.window_manager
        wm.event_timer_remove(self._timer)

def register():
    bpy.utils.register_class(RefreshTextureNode)
    bpy.utils.register_class(RefreshPreview)
    bpy.utils.register_class(RefreshBothOp)

def unregister():
    bpy.utils.unregister_class(RefreshTextureNode)
    bpy.utils.unregister_class(RefreshPreview)
    bpy.utils.unregister_class(RefreshBothOp)
