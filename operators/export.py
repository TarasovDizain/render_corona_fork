import bpy
import os, time, datetime, os.path
import cProfile
import hashlib
from ..util     import *
from ..export   import export
from ..outputs  import *

from xml.etree.cElementTree import Element, Comment, SubElement

EnableProfiling = False
#-----------------------------
# Corona export operator
#-----------------------------
class CoronaExportOperator(bpy.types.Operator):
    '''Export objects only'''
    bl_idname = "corona.export"
    bl_label = "Export"

    _inst_set = set()
    _psysob_set = set()
    _exported_obs = set()
    _exported_meshes = set()

    def execute(self, context):
        scene = context.scene
        start = time.time()

        # If exporting hair enabled, there's a good chance there will actually be particle systems to export.
        # A hair object placeholder is needed before iterating through scene objects. It is not renderable.
        placeholders = []
        result = None

        if scene.corona.export_hair:
            crv = bpy.data.curves.new('corona_hair_tmp_curve', 'CURVE')
            crv_ob = bpy.data.objects.new("%s_ob" % crv.name, crv)
            crv_ob.hide_render = True
            placeholders = [crv, crv_ob]
            if EnableProfiling:
                result = cProfile.runctx("export( self, scene, placeholders)", globals(), locals())
            else:
                result = export(self, scene, placeholders)
            # Clean up the scene
            bpy.data.objects.remove(crv_ob)
            bpy.data.curves.remove(crv)
        else:
            if EnableProfiling:
                result = cProfile.runctx("export( self, scene, placeholders)", globals(), locals())
            else:
                result = export(self, scene, placeholders)

        CrnInfo("Total export time: %.2f" % (time.time() - start))

        if result != None:
            CrnInfo("Failed to export: %s" % result)
            self.report({'WARNING'}, result)
            bpy.context.window_manager['corona'] = {'msg': result}
            return { 'CANCELLED' }

        return { 'FINISHED' }


#--------------------------------
# Operator for writing .mtl files
class CoronaMTLWrite(bpy.types.Operator):
    '''Export Corona .mtl file only'''
    bl_label = "Export Materials"
    bl_idname = "corona.export_mat"

    object_name = bpy.props.StringProperty(name="object name")
    object_file_path = bpy.props.StringProperty(name="object file path")
    mtlindex_creation = bpy.props.BoolProperty(False)
    exporting_empty_mtl = bpy.props.BoolProperty(True)

    def execute(self, context):
        textures_set = set()
        scene = context.scene

        mtlid = None
        if not self.object_name or not self.object_file_path:
            filename = name_compat(scene.name) + ".mtl"
            filepath = os.path.join(resolve_export_path(scene.corona), filename)
        else:
            filepath = self.object_file_path
            sha = hashlib.sha1()
            idkey = filepath.replace(resolve_export_path(scene.corona), "").replace('/','\\')
            sha.update(idkey.encode('utf8'))
            mtlid = sha.hexdigest()

        CrnUpdate("Writing material file ", filepath)

        # Collect the materials for export to .mtl
        materials = list()
        material_order = list()
        obj = None
        # rendertypes = ['MESH', 'SURFACE', 'META', 'TEXT', 'CURVE']

        if not self.object_name:
            for mat in bpy.data.materials:
                # if mat.users == 0:
                #     continue
                debug("Pre Material A", mat.name, mat.library.name if mat.library else None)

                # crn_mat = mat.corona
                # if crn_mat.node_tree != '' and crn_mat.node_output is not None:
                #     node_tree = crn_mat.node_tree
                #     try:
                #         ntree = bpy.data.node_groups[node_tree]
                #     except:
                #         if mat.library:
                #             with bpy.data.libraries.load(mat.library.filepath) as (data_from, data_to):
                #                 data_to.node_groups = [node_tree] # = data_from.node_groups[node_tree]
                #             ntree = bpy.data.node_groups[node_tree]
                #         else:
                #             debug("Appending probably is broken")
                if mat.name not in material_order:
                    material_order.append(mat.name)
                    materials.append((mat.name, mat.library.name if mat.library else None, mat))
        else:
            obj = context.blend_data.objects[self.object_name]
            if obj:
                instance_mats = get_instance_materials(obj, scene.corona)
                for matname in instance_mats:
                    try:
                        mat = bpy.data.materials[matname]
                        debug("Pre Material B", mat.name, mat.library.name if mat.library else None)
                        # crn_mat = mat.corona
                        # if crn_mat.node_tree != '' and crn_mat.node_output is not None:
                        #     node_tree = crn_mat.node_tree
                        #     try:
                        #         ntree = bpy.data.node_groups[node_tree]
                        #     except:
                        #         if mat.library:
                        #             with bpy.data.libraries.load(mat.library.filepath) as (data_from, data_to):
                        #                 data_to.node_groups = [node_tree] # = data_from.node_groups[node_tree]
                        #             ntree = bpy.data.node_groups[node_tree]
                        #         else:
                        #             debug("Appending might be broken")
                        if mat.name not in material_order:
                            material_order.append(mat.name)
                            materials.append((mat.name, mat.library.name if mat.library else None, mat))
                    except:
                        pass


        if len(materials) > 0 or self.exporting_empty_mtl:
            # Open the file and write header.
            mat_file = open(filepath, "wb")
            fw = mat_file.write

            root = Element('mtlLib')

            root.append(Comment('# Blender MTL File: %r' % (os.path.basename(bpy.data.filepath) or "None")))
            root.append(Comment('# Material Count: {}'.format(len(materials) if len(materials) > 0 else 1)))
            version = get_version_string()
            time_stamp = get_timestamp()
            root.append(Comment('# Generated by %s addon, %s, %s' % (script_name, version, time_stamp)))

            if not self.object_name:
                if scene.corona.material_override:
                    mat = None
                    default_mat = False
                    if scene.corona.clay_render:
                        default_mat = True
                    else:
                        debug(bpy.data.materials)
                        mat = bpy.data.materials[scene.corona.override_material]
                    write_mtl( root, default_mat, mat, 'Default_corona_blender', True )
                else:
                    write_mtl( root, True, None, 'Default_corona_blender', True )

            # Write the mtlindex before they are sorted
            if self.mtlindex_creation:
                print('Materials', materials)
                self.create_mtlindex(materials, mtlid)

            #Standard materials
            materials = sorted(materials, key=lambda m: m[0])
            self.append_mtls(materials, root, scene, mtlid)

            fw(prettify(root))

            mat_file.close()

        return {'FINISHED'}

    def append_mtls(self, materials, root, scene, mtlid):
        if len(materials) > 0:
            for mtl_mat_name, lib_name, mat in materials:
                debug("Material", lib_name, mtl_mat_name)
                default_mat = False
                name = mtl_mat_name if lib_name is None else lib_name + '_' + mtl_mat_name
                if mtlid:
                    name = name + '_' + mtlid
                else:
                    name = resolveMaterialName(name)
                write_mtl( root, default_mat, mat, name, True)

    def create_mtlindex(self, materials, mtlid):
        if len(materials) > 0:
            filepath = self.object_file_path.replace('.mtl', '.mtlindex')
            mtlindex_file = open(filepath, "w")
            for mtl_mat_name, lib_name, mat in materials:

                name = mtl_mat_name if lib_name is None else lib_name + '_' + mtl_mat_name
                if mtlid:
                    name = name + '_' + mtlid
                else:
                    name = resolveMaterialName(name)

                mtlindex_file.write(name + '\n')

            mtlindex_file.close()

#------------------------------------------
class CoronaSCNExport(bpy.types.Operator):
    '''Export Corona configuration files only'''
    bl_label = "Export Configuration"
    bl_idname = "corona.export_scene"

    # Local mode.
    _local_mode = False
    _local_layers = None

    # Collect dupli objects/instances.
    _dupli_obs = []
    _psys_obs = {}
    # Collect external .obj files used as instances.
    _ext_obs = []
    _ext_psys_obs = []
    # A set of all instanced objects.
    _no_export = set()
    # Exported objects for modifier detection
    _exported_obs = []

    def execute(self, context):
        scene = context.scene
        crn_scn = scene.corona
        camera = scene.camera

        scn_filename = name_compat(scene.name) + '.scn'
        conf_filename= name_compat(scene.name) + '.conf'
        try:
            scn_file = os.path.join(resolve_export_path(scene.corona), scn_filename)
            conf_file = os.path.join(resolve_export_path(scene.corona), conf_filename)
            mesh_dir = os.path.join( resolve_export_path(scene.corona), "meshes")
        except Exception as e:
            self.report({'ERROR'}, "Error creating export directory: " + str(e))
            return {'CANCELLED'}

        if not os.path.exists( mesh_dir):
            os.makedirs( mesh_dir, exist_ok = True)

        #Open and write the .conf file
        conf_success = write_conf(self, context, conf_file)
        if not conf_success:
            self.report({'ERROR'}, "Something went wrong, unable to write .conf file. Check directory permissions of export directory.")
            return {'CANCELLED'}

        # Check if using local mode.

        for window in context.window_manager.windows:
            screen = window.screen
            for area in screen.areas:
                if area.type == 'VIEW_3D':
                    space = area.spaces[0]
                    self._local_layers = space.layers_local_view[:]
                    break
        self._local_mode = self._local_layers is not None and True in self._local_layers

        # Open and write the .scn file.
        scn_success = write_scn( self, context, scn_file, conf_filename)
        if not scn_success:
            self.report({'ERROR'}, "Something went wrong, unable to write .scn file. Check directory permissions of export directory.")
            return {'CANCELLED'}

        return {'FINISHED'}

#------------------------------------------
class CoronaTextureCheck(bpy.types.Operator):
    '''Check all textures are available'''
    bl_label = "Check Textures"
    bl_idname = "corona.check_textures"

    def execute(self, context):
        scene = context.scene
        crn_scn = scene.corona

        missing = False
        for material in bpy.data.materials:

            if not material.use_nodes or not material.corona.use_nodes:
                continue
            node_tree = material.node_tree
            nodes = material.node_tree.nodes
            for node in nodes.values():
                if node.bl_idname != "CoronaTexNode":
                    continue
                tex_path = node.get_texture_path()
                if not tex_path:
                    continue
                tex_path = realpath(tex_path)
                if not os.path.exists(tex_path):
                    msg = "Missing texture %s.%s '%s'" % (material.name, node.name, tex_path)
                    self.report({'ERROR'}, msg)
                    missing = True
        # # Open and write the .scn file.
        # scn_success = write_scn( self, context, scn_file, conf_filename)
        # if not scn_success:
        #     self.report({'ERROR'}, "Something went wrong, unable to write .scn file. Check directory permissions of export directory.")
        #     return {'CANCELLED'}

        if not missing:
            self.report({'INFO'}, "Located all textures")

        return {'FINISHED'}

def register():
    bpy.utils.register_class( CoronaExportOperator)
    bpy.utils.register_class( CoronaMTLWrite)
    bpy.utils.register_class( CoronaSCNExport)
    bpy.utils.register_class( CoronaTextureCheck)
def unregister():
    bpy.utils.unregister_class( CoronaExportOperator)
    bpy.utils.unregister_class( CoronaMTLWrite)
    bpy.utils.unregister_class( CoronaSCNExport)
    bpy.utils.unregister_class( CoronaTextureCheck)
