import bpy
from bpy.app.handlers   import persistent

@persistent
def pre_ob_updated(scene):
    ob = scene.objects.active
    if ob is not None and ob.is_updated and not ob.corona.is_dirty:
        ob.corona.is_dirty = True

@persistent
def pre_ob_updated_data(scene):
    ob = scene.objects.active
    if ob is not None and ob.is_updated_data and not ob.corona.is_dirty:
        ob.corona.is_dirty = True

@persistent
def pre_ob_data_updated(scene):
    ob = scene.objects.active
    if ob is not None and ob.data is not None and ob.data.is_updated and not ob.corona.is_dirty:
        ob.corona.is_dirty = True

@persistent
def pre_ob_data_updated_data(scene):
    ob = scene.objects.active
    if ob is not None and ob.data is not None and ob.data.is_updated_data and not ob.corona.is_dirty:
        ob.corona.is_dirty = True

@persistent
def post_ob_updated(scene):
    ob = scene.objects.active
    if ob is not None and ob.is_updated and not ob.corona.is_dirty:
        ob.corona.is_dirty = True

@persistent
def post_ob_updated_data(scene):
    ob = scene.objects.active
    if ob is not None and ob.is_updated_data and not ob.corona.is_dirty:
        ob.corona.is_dirty = True

@persistent
def post_ob_data_updated(scene):
    ob = scene.objects.active
    if ob is not None and ob.data is not None and ob.data.is_updated and not ob.corona.is_dirty:
        ob.corona.is_dirty = True

@persistent
def post_ob_data_updated_data(scene):
    ob = scene.objects.active
    if ob is not None and ob.data is not None and ob.data.is_updated_data and not ob.corona.is_dirty:
        ob.corona.is_dirty = True

def register():
    bpy.app.handlers.scene_update_pre.append(pre_ob_updated)
    bpy.app.handlers.scene_update_pre.append(pre_ob_updated_data)
    bpy.app.handlers.scene_update_pre.append(pre_ob_data_updated)
    bpy.app.handlers.scene_update_pre.append(pre_ob_data_updated_data)

    bpy.app.handlers.scene_update_post.append(post_ob_updated)
    bpy.app.handlers.scene_update_post.append(post_ob_updated_data)
    bpy.app.handlers.scene_update_post.append(post_ob_data_updated)
    bpy.app.handlers.scene_update_post.append(post_ob_data_updated_data)


def unregister():
    pre = ['pre_ob_updated', 'pre_ob_updated_data', 'pre_ob_data_updated', 'pre_ob_data_updated_data']
    for f in bpy.app.handlers.scene_update_pre:
        if f.__name__ in pre:
            bpy.app.handlers.scene_update_pre.remove(f)

    post = ['post_ob_updated', 'post_ob_updated_data', 'post_ob_data_updated', 'post_ob_data_updated_data']
    for f in bpy.app.handlers.scene_update_post:
        if f.__name__ in post:
            bpy.app.handlers.scene_update_post.remove(f)