## Version 8.x In Progress ##

Version 8.x will make this plugin compatible with Blender 2.79 and Corona Renderer 3

## Version 9.x Not Yet Available ##

Version 9 will update to Blender 2.80+ and likely use Corona Renderer 3

This will require significant changes please be patient :)

## Tutorial ##

This is the latest tutorial about setting up this plugin to work with Blender and Corona.  http://www.chocofur.com/corona-renderer-and-blender-introduction.html

### Documentation ###

[Release Notes 2019 For Exporter](https://bitbucket.org/coronablender/render_corona/wiki/Release%20Notes%202019)

[Corona Standalone Documentation](https://corona-renderer.com/wiki/standalone)

### Forums ###

https://corona-renderer.com/forum/index.php/board,28.0.html

### Support ###

I am providing this plugin as a hobby, my profession is Software Development with an interest in 3D modelling. It wouldn't be possible without the support of the Corona team and those that help out in the forums and provide issues here :).

If you wish to provide monetry support the best way for me is via [Paypal](https://paypal.me/GlenBlanchard) or [Patreon](https://www.patreon.com/BlenderCorona).  

Supporters will be listed here as a thank you, send me a message if you would like a particular link to go against your name or you would like to remain Anonymous.

#### Supporters ####

Thank you!

* [Chocofur](https://chocofur.com)
* Arturo Rodriquez
* [AZR Studio]( http://www.azrstudio.nl/)
* Kristian
* Philip Kelly
* Ladislav Lacko
* Spencer Robertson
* Dex
* Ciro Vendrame
* Johannes Wilde
* Sanekum


This plugin is provided on top of work done by several other people.

OS | Corona Version | Blender Version | Exporter Version
---|----------------|-----------------|-----------------
Mac OS | [Standalone 3](https://drive.google.com/drive/folders/12bKiWzQPfvC5slZtJEjjV-Za2t1isgRV?usp=sharing) | [1.77-1.79](http://blender.org) | [v8.x.x](https://bitbucket.org/coronablender/render_corona/get/master.zip)
Windows | [Standalone 3](https://drive.google.com/drive/folders/12bKiWzQPfvC5slZtJEjjV-Za2t1isgRV?usp=sharing) | [1.77-1.79](http://blender.org) | [v8.x.x](https://bitbucket.org/coronablender/render_corona/get/master.zip)


### MacOS ###

**Material Preview problems if your material uses image textures or lights with IES**

Corona Path: /Applications/Corona Standalone.app/Contents/MacOS/Corona Standalone


### Windows ###

**Material Preview problems if your material uses image textures or lights with IES**

### Installation ###

Put into your blender/scripts/addons directory and configure using the installation steps from here: [Installation Instructions](https://corona-renderer.com/wiki/blender2corona/installation)

### Development ###

The easiest way to develop is to fork a copy and checkout.  Then create a branch and commit/push to that.  When you are ready then submit a pull request through bitbucket.

#### Testing ####

There is a way to test some of the functionality right now.  You need to have python installed somewhere (even the python included with blender is good enough)

Set the environment variable `CORONA=<path to corona standalone folder here>`

Then run `python tests.py "<path to blender.exe here>"`

This takes a little while to run and will test some of the basic functionality.

Additional tests can be added by creating a new subdirectory in `tests/<your dir>`
Include a `<yourtest>.test.py` and a `<yourtest>.blend`

See `tests/01 basic` for an example. Note that tests can only use the libraries that are included with blender.  There are helper functions for comparing plain text files, xml files and image files.