meta:
  id: cgeo
  file-extension: cgeo
  xref: https://corona-renderer.com/wiki/standalone/cgeo
seq:
  - id: header
    type: header
  - id: data
    type: data
types:
  header:
    seq:
      - id: magic
        contents: [67]
      - id: header_size
        type: u4le
      - id: header
        type: key_val
        size: header_size
  key_val:
    seq:
      - id: value
        type: str
        terminator: 0xA
        encoding: ASCII
        repeat: until
        repeat-until: _io.eof
        eos-error: false
  data:
    seq:
      - id: flags
        type: u4le
      - id: start
        type: u4le
        enum: tags
      - id: offsets
        type: offset
        repeat: until
        repeat-until: _.tag == tags::tag_anim_table_end
  offset:
    seq:
      - id: tag
        type: u4le
        enum: tags
      - id: msb
        type: u4le
        if: tag != tags::tag_anim_table_end
    instances:
      value:
        value: msb << 32 | tag.to_i
        if: tag != tags::tag_anim_table_end
      snapshot:
        if: tag != tags::tag_anim_table_end
        io: _root._io
        pos: value - 4
        type: snapshot
  snapshot:
    seq:
      - id: start
        type: u4le
        enum: tags
      - id: sections
        type: section
        repeat: until
        repeat-until: _.rec_type == tags::tag_animation_snapshot_end
  section:
    seq:
      - id: rec_type
        type: u4le
        enum: tags
      - id: len
        if: rec_type != tags::tag_animation_snapshot_end
        type: u8le
      - id: body
        if: rec_type != tags::tag_animation_snapshot_end
        size: len
        type:
          switch-on: rec_type
          cases:
            tags::tag_mesh_data: mesh_data
  mesh_data:
    seq:
      - id: flag
        type: u1
      - id: vertex_count
        type: u4le
      - id: normal_count
        type: u4le
      - id: map_count
        type: u4le
      - id: triangle_count
        type: u4le
      - id: vertices
        type: xyz
        repeat: expr
        repeat-expr: vertex_count
      - id: end_vertices
        contents: [0xCC, 0x42, 0xC3, 0x42]
      - id: normals
        type: xyz
        repeat: expr
        repeat-expr: normal_count
      - id: end_normals
        contents: [0xB0, 0x06, 0x0B, 0xB0]
      - id: map_coordinates
        type: map_coordinate
        repeat: expr
        repeat-expr: map_count
      - id: end_map_coordinates
        contents: [0xCA, 0x04, 0xAC, 0xCA]
      - id: triangles
        type: triangle
        repeat: expr
        repeat-expr: triangle_count
  triangle:
    seq:
      - id: vcount
        type: u4le
      - id: vertices
        type: u4le
        repeat: expr
        repeat-expr: 3 * (vcount + 1)
      - id: ncount
        type: u4le
      - id: normals
        type: u4le
        repeat: expr
        repeat-expr: 3 * (ncount + 1)
      - id: map_coordinates
        type: map_channel
        repeat: expr
        repeat-expr: _parent.map_count
      - id: material
        type: u2le
      - id: edge_visiblity
        type: u1
  map_channel:
    seq:
      - id: indices
        type: u4le
        repeat: expr
        repeat-expr: 3
  map_coordinate:
    seq:
      - id: count
        type: u4le
      - id: coordinates
        type: xyz
        repeat: expr
        repeat-expr: count
  xyz:
    seq:
      - id: x
        type: f4le
      - id: y
        type: f4le
      - id: z
        type: f4le
enums:
  tags:
    0xFAD0CCA1: tag_anim_table_start
    0xE580AC23: tag_anim_table_end
    0xA3CDEF00: tag_animation_snapshot_start
    0xFED3BA11: tag_animation_snapshot_end
    0x35F71EA8: tag_mesh_data
    0x42C342CC: tag_mesh_vertices_end
    0xB00B06B0: tag_mesh_normals_end
    0x2F89A8B5: tag_bounding_box
