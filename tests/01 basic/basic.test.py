import unittest
import bpy
import render_corona
from render_corona.tests import *

class TestBasic(unittest.TestCase):
    def test_export_scene(self):
        bpy.ops.corona.export_scene()
        self.assertTrue(CompareXml('tests/01 basic/Scene.scn', '.tmp/Scene.scn'))

    def test_export_material(self):
        bpy.ops.corona.export_mat()
        self.assertTrue(CompareXml('tests/01 basic/Scene.mtl', '.tmp/Scene.mtl'))

    def test_render(self):
        bpy.ops.render.render()
        # The image is as expected
        self.assertTrue(CompareImg('tests/01 basic/Scene1.png', '.tmp/render/Scene1.png', 5))

        # The cube is as expected
        self.assertTrue(CompareTextFiles('tests/01 basic/meshes/Cube.obj', '.tmp/meshes/Cube.obj'))
        self.assertTrue(CompareXml('tests/01 basic/meshes/Cube.mtl', '.tmp/meshes/Cube.mtl'))
        self.assertTrue(CompareTextFiles('tests/01 basic/meshes/Cube.mtlindex', '.tmp/meshes/Cube.mtlindex'))

        # The plane is as expected
        self.assertTrue(CompareTextFiles('tests/01 basic/meshes/Plane.obj', '.tmp/meshes/Plane.obj'))
        self.assertTrue(CompareXml('tests/01 basic/meshes/Plane.mtl', '.tmp/meshes/Plane.mtl'))
        self.assertTrue(CompareTextFiles('tests/01 basic/meshes/Plane.mtlindex', '.tmp/meshes/Plane.mtlindex'))

RunTests(TestBasic)